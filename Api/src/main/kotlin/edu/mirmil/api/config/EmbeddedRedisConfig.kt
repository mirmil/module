package edu.mirmil.api.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import redis.embedded.RedisServer
import java.io.IOException
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Configuration
class EmbeddedRedisConfig {

    @Value("\${spring.redis.port:#{6379}}")
    private lateinit var redisPort: Number

    private lateinit var redisServer: RedisServer

    @PostConstruct
    @Throws(IOException::class)
    fun redisServer() {
        redisServer = RedisServer(redisPort.toInt())
        redisServer.start()
    }

    @PreDestroy
    fun stopRedis() {
        if (redisServer.isActive) redisServer.stop()
    }
}