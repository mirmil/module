package edu.mirmil.api.utils

import org.springframework.cache.interceptor.KeyGenerator
import java.lang.reflect.Method

class CustomKeyGen : KeyGenerator {
    override fun generate(target: Any, method: Method, vararg params: Any?): Any {
        return "${target.javaClass.simpleName}_${method.name}_${params.joinToString("_")}"
    }
}