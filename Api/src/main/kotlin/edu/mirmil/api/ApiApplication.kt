package edu.mirmil.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication(scanBasePackages = ["edu.mirmil.api", "edu.mirmil.domain"])
@EntityScan(basePackages = ["edu.mirmil.domain.entity"])
@EnableJpaRepositories(basePackages = ["edu.mirmil.domain.repository"])
@EnableCaching
class ApiApplication

fun main(args: Array<String>) {
    runApplication<ApiApplication>(*args)
}