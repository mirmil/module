plugins {
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    war
    id("org.asciidoctor.jvm.convert")
}

val snippetsDir by extra { file("build/generated-snippets") }

tasks {
    test {
        outputs.dir(snippetsDir)
        useJUnitPlatform()
    }

    asciidoctor {
        asciidoctorj.attribute("snippets", snippetsDir)
        dependsOn(test)
        baseDirFollowsSourceFile()
        setOutputDir("src/main/resources/static/docs")
    }

    build {
        dependsOn(asciidoctor)
    }
}

val developmentOnly: Configuration by configurations.creating
configurations {
    runtimeClasspath {
        extendsFrom(developmentOnly)
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
//    implementation("org.thymeleaf:thymeleaf-spring4")

    developmentOnly("org.springframework.boot:spring-boot-devtools")

    testImplementation("org.springframework.restdocs", "spring-restdocs-mockmvc")

    //test
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
        exclude(module = "junit")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}